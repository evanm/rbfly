#!/usr/bin/env Rscript
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Make plot to compare performance of sending data to RabbitMQ Streams
# broker using binary string and AMQP encoded data.
# 
# See `run-perf-amqp` script for more details.
#

library(ggplot2)
library(dplyr)
library(tidyr)

args = commandArgs(trailingOnly=T)
if (length(args) == 2) {
    file_in = args[1]
    file_out = args[2]
} else {
    cat('usage: plot-perf-pub-amqp <input> <output>\n')
    quit('no', 1)
}

data = read.csv(file_in)
data$batch = factor(data$batch)
levels(data$batch) <- c('single', 'batch')

data.summary = (
    data
    %>% group_by(batch, type)
    %>% summarise(speed=median(speed))
    %>% pivot_wider(names_from=type, values_from=speed)
    %>% mutate(pct=((amqp - binary) / binary) * 100)
)
print(data.summary)

p = (
      ggplot(data, aes(type, speed / 1000)) + geom_boxplot(notch=T)
      + facet_wrap(~batch, ncol=2, scales='free')
      + xlab(NULL)
      + ylab('Throughput [thousand messages / sec]')
      + theme_minimal()
      + theme(
            text=element_text(size=12),
            axis.title=element_text(size=10)
        )
)

if (endsWith(file_out, '.png')) {
    ggsave(file_out, plot=p, width=1000, height=400, units='px', dpi=140)
} else if (endsWith(file_out, '.pdf')) {
    ggsave(file_out, plot=p, width=15, height=8, units='cm', dpi=600)
} else {
    stop('invalid format of plot file, use png or pdf')
}

# vim: sw=4:et:ai
