#!/usr/bin/env python
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import csv
import itertools
import proton  # type: ignore
import sys
import timeit
import typing as tp
import uamqp  # type: ignore

from rbfly.amqp._message import MessageCtx, encode_amqp, decode_amqp
from rbfly.streams import AMQPBody

TestFunc: tp.TypeAlias = tp.Callable[[], tp.Any]

flatten = itertools.chain.from_iterable

BUFF = bytearray(10 ** 6)
DATA: AMQPBody = {
    'large-int': 2 ** 32 + 1,
    'small-list': [1, 2, 3],
    'bin-string': b'abc',
}

size = encode_amqp(BUFF, MessageCtx(DATA))
DATA_BIN = bytes(BUFF[:size])

def rbfly_encode() -> bytearray:
    size = encode_amqp(BUFF, MessageCtx(DATA))
    return BUFF[:size]

def rbfly_decode() -> MessageCtx:
    return decode_amqp(DATA_BIN)

def proton_encode() -> bytes:
    return proton.Message(body=DATA).encode()  # type: ignore

def proton_decode() -> proton.Message:
    msg = proton.Message()
    msg.decode(DATA_BIN)
    return msg

def uamqp_encode() -> bytes:
    return uamqp.Message(body=DATA).encode_message()  # type: ignore

def uamqp_decode() -> uamqp.Message:
    return uamqp.Message.decode_from_bytes(DATA_BIN)

def run_test(func: TestFunc) -> float:
    # report in microseconds, per single item
    n = 10 ** 5
    return timeit.timeit(func, number=n) / n * 1e6

projects = {
    'rbfly': (rbfly_encode, rbfly_decode),
    'proton': (proton_encode, proton_decode),
    'uamqp': (uamqp_encode, uamqp_decode),
}

# check encode and decoder functions
for n, (fe, fd) in projects.items():
    try:
        data = fe()
        assert len(data) > 10

        msg = fd()
        assert isinstance(DATA, dict)
        assert set(msg) == set(DATA.keys())
    except:
        print('Codec failure for {}'.format(n), file=sys.stderr)
        raise

# run tests
data = flatten((  # type: ignore
    ((n, 'encode', run_test(fe)), (n, 'decode', run_test(fd)))
    for n, (fe, fd) in projects.items()
))

writer = csv.writer(sys.stdout)
writer.writerow(['project', 'operation', 'time'])
writer.writerows(data)  # type: ignore

# vim: sw=4:et:ai
