#!/usr/bin/env python3
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
RbFly demo to perform CPU bound calculations with NumPy, and asyncio thread
executor.

The RbFly avoids using threads internally. Decision to use threads is left
up to an application as threads need to be managed, and bring additional
cost like context switching and increased memory consumption.

If an application performs a long, CPU bound calculation, then RbFly is
prevented from sending heartbeats to RabbitMQ Streams broker, and the
broker will close connection. RbFly will reconnect once application yields
control to asyncio loop. This scenario can be simulated with `-s` parameter
of this script.

The disconnection problem can be avoided by running calculations in
a thread. It might not work if calculations are done in Python because
of GIL (https://docs.python.org/3/glossary.html#term-global-interpreter-lock).
However, libraries like NumPy might release GIL when starting calculations.

This demo consumes messages from a stream, and performs matrix
multiplication with NumPy. The calculation lasts few minutes (if using
faster CPU, increase size of a matrix with `-n` parameter to perform
a calculation for at least 3 minutes), and is executed in asyncio thread
executor.

While the calculation is performed

- the demo reads messages from the stream with other consumer
- RbFly is sending heartbeats to RabbitMQ Streams broker

32 GB or RAM required to run this demo comfortably.

TODO: Put above as topic article into the documentation.
"""

import argparse
import asyncio
import logging
import numpy as np
from datetime import datetime
from functools import partial

import rbfly.streams as rbs

STREAM = 'rbfly-demo-stream-cpubound'

async def send_data(client: rbs.StreamsClient) -> None:
    async with client.publisher(STREAM) as publisher:
        for _ in range(3600):
            await publisher.send('hello')

async def receive_data(client: rbs.StreamsClient) -> None:
    num_msg = 0
    print('{} start receiving messages'.format(datetime.now()))
    offset = rbs.Offset.FIRST
    async for msg in client.subscribe(STREAM, offset=offset):
        num_msg += 1
        await asyncio.sleep(1)
        if num_msg % 60 == 0:
            print('{} done receiving messages, num={}'.format(
                datetime.now(), num_msg
            ))

async def read_and_calculate(
        client: rbs.StreamsClient,
        use_threads: bool,
        size: int
) -> None:
    loop = asyncio.get_event_loop()
    offset = rbs.Offset.FIRST
    async for msg in client.subscribe(STREAM, offset=offset):
        if use_threads:
            await loop.run_in_executor(None, calculate, size)
        else:
            calculate(size)
        await asyncio.sleep(1)

def calculate(size: int):
    print('{} starting calculation'.format(datetime.now()))
    matrix = np.random.rand(size, size)
    result = matrix @ matrix @ matrix
    print('{} done calculating'.format(datetime.now()))

@rbs.connection
async def demo(client: rbs.StreamsClient, use_threads: bool, size: int) -> None:
    try:
        await client.create_stream(STREAM)
        await send_data(client)
        await asyncio.gather(
            read_and_calculate(client, use_threads, size),
            receive_data(client)
        )
    finally:
        await client.delete_stream(STREAM)

parser = argparse.ArgumentParser()
parser.add_argument(
    '-s', '--no-threads', action='store_true', default=False,
    help='use threads to perform cpu bound calculation'
)
parser.add_argument(
    '-n', '--size', default=24 * 1024, type=int,
    help='size of matrix used for calculation'
)
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)
client = rbs.streams_client('rabbitmq-stream://guest:guest@localhost')
use_threads = not args.no_threads

asyncio.run(demo(client, use_threads, args.size))

# vim: sw=4:et:ai
