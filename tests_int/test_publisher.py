#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Integration tests for publishing messages.

TODO: Maybe use `StreamStats` RabbitMQ Streams protocol command to verify
the published messages.

NOTE: Tests assume 1024 ** 2 frame size.

TODO: Add unit test to publish with large message id values (i.e. 2 ** 31,
      2 ** 32).
"""

import rbfly.streams as rbs
import typing as tp
from contextlib import AbstractAsyncContextManager

import pytest
from conftest import Client, _stream_subscribe, stream_read

Publisher: tp.TypeAlias = AbstractAsyncContextManager[rbs.Publisher]
PublisherBatch: tp.TypeAlias = AbstractAsyncContextManager[rbs.PublisherBatchFast]

@pytest.mark.asyncio
async def test_publish_large(stream_pub_no_batch: Publisher) -> None:
    """
    Test publishing very large message, close to frame size.
    """
    async with stream_pub_no_batch as p:
        await p.send(b'1' * (1024 ** 2 - 33))

@pytest.mark.asyncio
async def test_publish_batch_many_small(
        stream_pub_batch: PublisherBatch
) -> None:
    """
    Test publishing many small messages, over frame size.
    """
    async with stream_pub_batch as publisher:
        for _ in range(256 * 1024):
            publisher.batch('1234')
        await publisher.flush()

@pytest.mark.asyncio
async def test_publish_batch_few_large(
        stream_pub_batch: PublisherBatch
) -> None:
    """
    Test publishing few large messages, over frame size.
    """
    async with stream_pub_batch as publisher:
        for _ in range(4):
            publisher.batch('1' * 256 * 1024)
        await publisher.flush()

@pytest.mark.asyncio
async def test_publish_batch_single_large(
        stream_pub_batch: PublisherBatch
) -> None:
    """
    Test publishing single large messages, over frame size.
    """
    # note: there if flush on exit, so pytest.raises needs to be outer
    # block
    with pytest.raises(ValueError) as ex_ctx:
        async with stream_pub_batch as publisher:
            publisher.batch('a' * 1024 ** 2)
            await publisher.flush()

    assert str(ex_ctx.value) == 'Message data too long'

@pytest.mark.asyncio
async def test_publish_batch_zero(stream_pub_batch: PublisherBatch) -> None:
    """
    Test publishing zero messages.
    """
    async with stream_pub_batch as publisher:
        await publisher.flush()

@pytest.mark.asyncio
async def test_publish_dedup(
        rbfly_client: Client,
        stream_pub_no_batch: Publisher,
) -> None:
    """
    Test publishing messages with deduplication.
    """
    async with stream_pub_no_batch as p:
        for i in range(10):
            ctx = rbs.stream_message_ctx(i, publish_id=i)
            await p.send(ctx)

        # these messages are ignored because of deduplication
        for i in range(10):
            ctx = rbs.stream_message_ctx(100 + i, publish_id=i)
            await p.send(ctx)

        offset = rbs.Offset.FIRST
        stream = await _stream_subscribe(rbfly_client, p.stream, offset=offset)
        messages = [msg async for msg in stream_read(stream)]
        assert messages == list(range(10))

@pytest.mark.asyncio
async def test_publish_fast_batch_close(
        rbfly_client: Client,
) -> None:
    """
    Test flushing messages on publisher close (fast).
    """
    name = 'rbfly.integration.batch.close'
    await rbfly_client.create_stream(name)
    try:
        async with rbfly_client.publisher(name, cls=rbs.PublisherBatchFast) as publisher:
            for i in range(12):
                publisher.batch(i)

        stream = await _stream_subscribe(rbfly_client, name)
        messages = [msg async for msg in stream_read(stream)]
        assert messages == list(range(12))
    finally:
        await rbfly_client.delete_stream(name)

@pytest.mark.asyncio
async def test_publish_limit_batch_close(
        rbfly_client: Client,
) -> None:
    """
    Test flushing messages on publisher close (limited batch).
    """
    name = 'rbfly.integration.batch.close'
    await rbfly_client.create_stream(name)
    try:
        async with rbfly_client.publisher(name, cls=rbs.PublisherBatchFast) as publisher:
            for i in range(13):
                publisher.batch(i)

        stream = await _stream_subscribe(rbfly_client, name)
        messages = [msg async for msg in stream_read(stream)]
        assert messages == list(range(13))
    finally:
        await rbfly_client.delete_stream(name)

# vim: sw=4:et:ai
