#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Integration tests for reading RabbitMQ stream messages.
"""

import asyncio
import logging
import time
import rbfly.streams as rbs
from functools import partial

import pytest
from conftest import DEFAULT_STREAM_SMALL, DEFAULT_STREAM_LARGE, Client, \
        Stream, pt_lazy, stream_read, _stream_populate_batch, _stream_subscribe

logger = logging.getLogger(__name__)

DEFAULT_TESTS_STREAMS_LARGE = (
    (pt_lazy('stream_large_no_batch'), DEFAULT_STREAM_LARGE),
    (pt_lazy('stream_large_batch'), DEFAULT_STREAM_LARGE),
)

DEFAULT_TESTS_STREAMS = (
    (pt_lazy('stream_small_no_batch'), DEFAULT_STREAM_SMALL),
    (pt_lazy('stream_small_batch'), DEFAULT_STREAM_SMALL),
    *DEFAULT_TESTS_STREAMS_LARGE
)

@pytest.mark.asyncio
@pytest.mark.parametrize('stream, size', DEFAULT_TESTS_STREAMS)
async def test_message_body(stream: Stream, size: int) -> None:
    """
    Test stream message body.
    """
    result = [msg async for msg in stream_read(stream)]
    assert result == list(range(size))

@pytest.mark.asyncio
@pytest.mark.parametrize('stream, size', DEFAULT_TESTS_STREAMS)
async def test_message_context_offset(stream: Stream, size: int) -> None:
    """
    Test stream offset of stream messages.
    """
    messages = [rbs.get_message_ctx() async for msg in stream_read(stream)]
    offsets = [m.stream_offset for m in messages]
    assert offsets == list(range(size))

@pytest.mark.asyncio
@pytest.mark.parametrize('stream, size', DEFAULT_TESTS_STREAMS)
async def test_message_context_timestamp(
        start_time: float, stream: Stream, size: int
) -> None:
    """
    Test stream timestamp of stream messages.
    """
    ts_start = start_time

    messages = [rbs.get_message_ctx() async for msg in stream_read(stream)]
    assert len(messages) == size

    time.sleep(0.1)  # so we can use '>' operator safely
    ts_end = time.time()

    timestamps = [m.stream_timestamp for m in messages]
    assert all(ts_start < ts < ts_end for ts in timestamps), \
        'start={}, end={}, range=({}, {})'.format(
            ts_start, ts_end, min(timestamps), max(timestamps)
        )

@pytest.mark.asyncio
async def test_offset_subscription(rbfly_client: Client) -> None:
    """
    Test batched stream offset subscription in the middle of a batch.
    """
    client = rbfly_client
    name = 'rbfly.integration.small.batch.offset'
    ctx = _stream_populate_batch(
        # each batch has 10 items
        client, name, range(DEFAULT_STREAM_SMALL), 10
    )

    # subscribe in the middle of a batch
    start = DEFAULT_STREAM_SMALL - 5
    async with ctx:
        stream = await _stream_subscribe(
            client,
            name,
            offset=rbs.Offset.offset(start),
        )
        values = [m async for m in stream_read(stream)]
        expected = list(range(start, DEFAULT_STREAM_SMALL))
        assert values == expected

@pytest.mark.asyncio
async def test_message_slow(stream_large_no_batch: Stream) -> None:
    """
    Test stream reading with sleepy reader.

    The slow stream consumer sleeps for a fraction of a second after
    consuming a message. The test depletes initial credit on RabbitMQ
    broker side and checks if RbFly can recover from this.
    """
    count = 0
    async for _ in stream_read(stream_large_no_batch):
        await asyncio.sleep(0.01)
        count += 1

    # check if all messages are read
    assert count == DEFAULT_STREAM_LARGE

@pytest.mark.asyncio
@pytest.mark.parametrize('stream, size', DEFAULT_TESTS_STREAMS_LARGE)
async def test_message_blocker(
        rbfly_client: Client,
        stream: Stream,
        size: int
) -> None:
    """
    Test reconnection of stream consumers, which block asyncio loop.

    Run two stream consumers, and read messages one by one. Stream offset
    of messages in a pair shall match. Block asyncio loop with
    `time.sleep`, it needs to last about three heartbeats. Then, RabbitMQ
    will disconnect RbFly client. After reconnection, continue checking
    stream offsets.
    """
    message_count = 0
    n_break = 5000
    assert n_break < 0.6 * size

    subscribe = partial(
        _stream_subscribe,
        rbfly_client,
        stream.name,
        offset=rbs.Offset.FIRST,
        timeout=0,
    )
    s1 = await subscribe()
    s2 = await subscribe()
    while True:
        await anext(s1.messages)
        m1 = rbs.get_message_ctx()
        await anext(s2.messages)
        m2 = rbs.get_message_ctx()

        message_count += 1
        if message_count == size:
            break

        assert m1.stream_offset == m2.stream_offset
        if message_count == n_break:
            # TODO: minimize waiting time by changing hearbeat to lower
            # value
            delay = 180
            logger.info('blocking asyncio loop for {} seconds'.format(delay))
            time.sleep(delay)

    assert message_count == size

# vim: sw=4:et:ai
