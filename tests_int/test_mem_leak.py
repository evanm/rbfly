#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Integration tests for possible memory leaks when reading messages from
a RabbitMQ stream.
"""

import asyncio
import logging

from rbfly.streams.const import INITIAL_CREDIT, QUEUE_MAX_SIZE

import pytest
from conftest import Client, Stream, pt_lazy, stream_read

logger = logging.getLogger(__name__)

STREAM_MEM_LEAK = (
    (pt_lazy('stream_large_no_batch'), QUEUE_MAX_SIZE + INITIAL_CREDIT),
    (pt_lazy('stream_large_batch'), QUEUE_MAX_SIZE + INITIAL_CREDIT * 11),
)

@pytest.mark.asyncio
@pytest.mark.parametrize('stream, size', STREAM_MEM_LEAK)
async def test_subscription_queue_size(
        rbfly_client: Client,
        stream: Stream,
        size: int,
) -> None:
    """
    Test possible subscription queue memory leak when processing RabbitMQ
    stream.
    """
    # ruff: noqa: SLF001
    count = 0
    logger.info('memory leak detection: start processing stream')
    async for _ in stream_read(stream):
        count += 1
        # test invariant
        assert len(list(rbfly_client._subscribers.items())) == 1

        if count == 10:
            logger.info('memory leak detection: long, 2s sleep')
            await asyncio.sleep(2)
            logger.info('memory leak detection: long sleep done')
        else:
            await asyncio.sleep(0.001)

        queue = rbfly_client._subscribers[0].subscriber.queue.data

        # to trigger a failure, comment out checks for queue size threshold
        # in rbfly.streams._mqueue module
        assert len(queue) < size

# vim: sw=4:et:ai
