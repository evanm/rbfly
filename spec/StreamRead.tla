----------------------------- MODULE StreamRead -----------------------------
(***************************************************************************)
(* Specification for a client reading messages from a RabbitMQ stream.     *)
(*                                                                         *)
(* This specification assumes                                              *)
(*                                                                         *)
(* - the client is a library, which reads messages from a stream using     *)
(*   RabbitMQ Streams protocol; consuming messages by an application is    *)
(*   implied                                                               *)
(* - there is unlimited supply of messages; arrival of messages to         *)
(*   a stream is not modeled                                               *)
(* - one chunk of messages contains single message; no batching;           *)
(*   "chunk of messages" equals "message" below                            *)
(*                                                                         *)
(* The client subscribes to a RabbitMQ stream with initial credit value,   *)
(* and starts receiving messages (up to that credit value). To keep        *)
(* receiving messages, the client needs to keep sending credit requests.   *)
(*                                                                         *)
(* Client puts received messages into a queue. An application consumes the *)
(* messages from the queue in an asynchronous way (the queue can receive   *)
(* new messages, when the application consumes messages from the queue).   *)
(*                                                                         *)
(* When the queue is full, then the client keeps putting received messages *)
(* into the queue, but stops sending credit requests. When the queue       *)
(* becomes empty, after being full, the client restarts sending credit     *)
(* requests.                                                               *)
(*                                                                         *)
(* The client maintains credit value at the initial credit value. If the   *)
(* initial value is 32, and five messages are received, then the client    *)
(* sends credit request with value 5.                                      *)
(***************************************************************************)

EXTENDS Naturals

CONSTANTS
    InitialCredit,   \* The initial credit value when subscribing to a stream.
    BrokerSendLimit, \* Broker send limit,
                     \* see https://github.com/rabbitmq/rabbitmq-server/blob/v3.11.8
                     \*     /deps/rabbitmq_stream/src/rabbit_stream_reader.erl#L1963.
    QueueMaxSize     \* Maximum size of client message queue (soft limit).

ASSUME /\ InitialCredit > 0
       /\ QueueMaxSize > InitialCredit
       /\ BrokerSendLimit < InitialCredit

VARIABLES
    queueSize,        \* Current size of client message queue, the queue is
                      \* emulated with a number of messages in the queue.
    creditDelta,      \* Credit value requested by client.
    credit,           \* Current credit value as tracked by the client.
    brokerCredit,     \* Current credit value as tracked by the broker.
    messagesInFlight, \* Number of messages being sent by the broker to the client.
    asyncRequests     \* Variable to simulate asynchronous requests.

vars == <<queueSize, creditDelta, credit, brokerCredit, messagesInFlight,
          asyncRequests>>
-----------------------------------------------------------------------------
CreditInvariant ==        \* All credit variables are non-negative numbers.
    /\ creditDelta >= 0
    /\ credit >= 0
    /\ credit <= InitialCredit
    /\ brokerCredit >= 0

MessageInvariant ==
    /\ messagesInFlight >= 0
    /\ messagesInFlight <= credit         \* There cannot be more messages sent than
                                          \* the current credit value.

QueueInvariant ==
    /\ queueSize >= 0       
    /\ queueSize <= QueueMaxSize + InitialCredit \* The client queue can be overfilled,
                                                 \* but there is a limit for the number
                                                 \* of received messages.
-----------------------------------------------------------------------------
AsyncRequest(request) ==             \* Simulation of an asynchronous request.
    /\ request \notin asyncRequests  \* There is only one given request in progress.
    /\ asyncRequests' = asyncRequests \union {request}

AsyncDone(request) ==    \* Mark an asynchronous request as done (or do nothing).
    /\ asyncRequests' = asyncRequests \ {request}
-----------------------------------------------------------------------------
RequestCredit ==          \* The client sends credit request to the broker. Maintain credit
    /\ creditDelta = 0    \* level at the initial credit value.
    /\ creditDelta' = IF queueSize < QueueMaxSize
                      THEN InitialCredit - credit  \* Credit value 1 leads to deadlock.
                      ELSE 0

UpdateCredit ==         \* Update the client and broker credit values. This is single
    /\ creditDelta > 0  \* step from the point of the client.
    /\ credit' = credit + creditDelta
    /\ brokerCredit' = brokerCredit + creditDelta
    /\ creditDelta' = 0
    /\ UNCHANGED <<queueSize, messagesInFlight, asyncRequests>>

SendMessage ==    \* The broker sends messages to a client.
    /\ brokerCredit >= BrokerSendLimit
    /\ messagesInFlight' = messagesInFlight + brokerCredit
    /\ brokerCredit' = 0
    /\ UNCHANGED <<queueSize, credit, creditDelta, asyncRequests>>

ReceiveMessage ==            \* The client receives messages from
    /\ messagesInFlight > 0  \* the broker, and sends credit requests.
    /\ messagesInFlight' = messagesInFlight - 1
    /\ queueSize' = queueSize + 1
    /\ credit' = credit - 1
    /\ RequestCredit
    /\ AsyncDone("message")
    /\ UNCHANGED brokerCredit

ReadMessage ==
    \/ /\ queueSize > 0               \* The client reads messages from the queue.
       /\ queueSize' = queueSize - 1  \* The messages are asynchronously consumed
                                      \* by an application.
       /\ UNCHANGED <<creditDelta, credit, brokerCredit, messagesInFlight,
                      asyncRequests>>

    \/ /\ queueSize = 0               \* If queue is empty,
       /\ credit < InitialCredit      \* and there is capacity to receive more messages,
       /\ RequestCredit               \* then request more credit.
       /\ AsyncRequest("message")     \* Send the request once, and wait for new messages.
       /\ UNCHANGED <<queueSize, messagesInFlight, credit, brokerCredit>>
-----------------------------------------------------------------------------
Init == /\ queueSize = 0
        /\ creditDelta = 0
        /\ credit = InitialCredit        \* The client subscribes to a stream
        /\ brokerCredit = InitialCredit  \* with the initial credit value.
        /\ messagesInFlight = 0
        /\ asyncRequests = {}

Next == SendMessage \/ ReceiveMessage \/ ReadMessage \/ UpdateCredit
Spec == Init /\ [][Next]_<<vars>>
=============================================================================
