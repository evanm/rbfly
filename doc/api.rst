RabbitMQ Streams API
====================
Broker Connection API
---------------------
.. autosummary::
   :nosignatures:

   rbfly.streams.streams_client
   rbfly.streams.connection
   rbfly.streams.StreamsClient

.. autofunction:: rbfly.streams.streams_client
.. autofunction:: rbfly.streams.connection

.. autoclass:: rbfly.streams.StreamsClient
   :members:

Publisher and Subscriber API
----------------------------
.. autosummary::
   :nosignatures:

   rbfly.streams.stream_message_ctx
   rbfly.streams.get_message_ctx
   rbfly.streams.StreamsClient
   rbfly.streams.Publisher
   rbfly.streams.PublisherBatchLimit
   rbfly.streams.PublisherBatchFast
   rbfly.streams.MessageCtx
   rbfly.streams.Offset

.. autofunction:: rbfly.streams.stream_message_ctx
.. autofunction:: rbfly.streams.get_message_ctx

.. autoclass:: rbfly.streams.Publisher
   :members:

   .. autoattribute:: name
   .. autoattribute:: stream
   .. autoattribute:: message_id

.. autoclass:: rbfly.streams.PublisherBatchLimit
   :members: batch, flush

   .. autoattribute:: name
   .. autoattribute:: stream
   .. autoattribute:: message_id

.. autoclass:: rbfly.streams.PublisherBatchFast
   :members: batch, flush

   .. autoattribute:: name
   .. autoattribute:: stream
   .. autoattribute:: message_id

.. autoclass:: rbfly.streams.MessageCtx

.. autoclass:: rbfly.streams.Offset
   :members:

Data Types
----------

.. autosummary::
   :nosignatures:

   rbfly.types.Symbol
   rbfly.types.AMQPScalar
   rbfly.types.AMQPBody
   rbfly.types.AMQPAppProperties

.. autoclass:: rbfly.types.Symbol
.. autodata:: rbfly.types.AMQPScalar
.. autodata:: rbfly.types.AMQPBody
.. autodata:: rbfly.types.AMQPAppProperties

Deprecated API
--------------
.. deprecated:: 0.7.0

   .. autoclass:: rbfly.streams.PublisherBatch

      Use :py:class:`rbfly.streams.PublisherBatchFast` class instead.

   .. autoclass:: rbfly.streams.PublisherBatchMem

      Use :py:class:`rbfly.streams.PublisherBatchLimit` class instead.

.. vim: sw=4:et:ai
