import sys
import os.path
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('..'))

import rbfly

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.autosummary', 'sphinx.ext.doctest',
    'sphinx.ext.todo', 'sphinx.ext.viewcode', 'sphinx.ext.intersphinx',
]
project = 'RbFly'
source_suffix = '.rst'

title = 'RbFly - a library for RabbitMQ Streams using Python asyncio'
version = release = rbfly.__version__
author = copyright = 'Artur Wroblewski'

if tags.has('html'):
    rbfly_report_ref = 'see :numref:`sec-performance`'
else:
    rbfly_report_ref = ':numref:`sec-documentation` references appropriate report'

rst_epilog = """
.. |rbfly-perf-stmt| replace:: over 3.5 times faster
.. |rbfly-lib| replace:: {uri}/rbfly-{ver}.pdf
.. |rbfly-performance| replace:: {uri}/rbfly-performance-{ver}.pdf
.. |rbfly-report-ref| replace:: {ref}
""".format(
    ver=version,
    uri='https://wrobell.dcmod.org/rbfly',
    ref=rbfly_report_ref
)

epub_basename = 'RbFly - {}'.format(version)
epub_author = 'Artur Wroblewski'

numfig = True
numfig_format = {
    "figure": 'Figure %s',
}
todo_include_todos = True

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_domain_indices = False
html_css_files = [
    'css/rbfly.css',
]

latex_table_style = ['booktabs']
latex_documents = [
    (
        'rbfly', 'rbfly-{}.tex'.format(version), 'RbFly Library',
        author, 'howto', False
    ),
    (
        'performance', 'rbfly-performance-{}.tex'.format(version),
        'RbFly Library Performance Report',
        author, 'howto', False
    ),
]
latex_elements = {
    'papersize': 'a4paper',
}
latex_domain_indices = False

autodoc_type_aliases = {
    'AMQPBody': 'rbfly.streams.types.AMQPBody'
}

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
}

# vim: sw=4:et:ai
