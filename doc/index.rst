RbFly - RabbitMQ Client Library
===============================
RbFly is a library for `RabbitMQ Streams <https://www.rabbitmq.com/streams.html>`_
using `Python asyncio <https://docs.python.org/3/library/asyncio.html>`_.

.. toctree::
   :caption: RbFly Library
   :numbered:

   overview
   license
   quick

.. toctree::
   :caption: Topics
   :numbered:

   publish
   subscribe
   connmgmt
   pubdedup
   streammgmt
   performance

.. toctree::
   :caption: Reference
   :numbered:

   api
   conn
   encoding
   changelog

* :ref:`genindex`

.. vim: sw=4:et:ai
