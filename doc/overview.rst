Overview
========
RbFly is a library for `RabbitMQ Streams <https://www.rabbitmq.com/streams.html>`_
using `Python asyncio <https://docs.python.org/3/library/asyncio.html>`_.

Features
--------
The library is designed and implemented with the following qualities in
mind

#. Simple, flexible, and asynchronous Pythonic API with type annotations.
#. Use of AMQP 1.0 message format to enable interoperability between
   RabbitMQ Streams clients.
#. Performance. For example, RbFly can be |rbfly-perf-stmt| than
   other, similar solutions when publishing messages to a streaming broker
   (|rbfly-report-ref|).
#. Auto reconnection to RabbitMQ broker with lazily created connection
   objects.

RbFly supports many RabbitMQ Streams broker features

#. Publishing single messages, or in batches, with confirmation.
#. Subscribing to a stream at a specific point in time, from a specific
   offset, or using offset reference.
#. Writing stream offset reference.
#. Message deduplication.
#. Integration with AMQP 1.0 ecosystem at message format level.

Requirements
------------
RbFly requires

- Python 3.10, or later
- RabbitMQ 3.11.17, or later
- Cython 3.0.0 (for development)

Installation
------------
Install or upgrade RbFly from `PyPi <https://pypi.org/project/rbfly/>`_
with `pip command
<https://packaging.python.org/en/latest/tutorials/installing-packages/#installing-from-pypi>`_::

    $ pip install -U rbfly

.. _sec-documentation:

Documentation
-------------
The documentation of RbFly library is hosted at project's website at the
address

    https://wrobell.dcmod.org/rbfly/

The documentation consists of multiple parts. Each part can be downloaded
in PDF format

================================ ===================
Document                                 URL
================================ ===================
RbFly Library                    |rbfly-lib|
RbFly Library Performance Report |rbfly-performance|
================================ ===================

Links
-----
#. RbFly project website: https://wrobell.dcmod.org/rbfly/index.html
#. Report bugs for RbFly project: https://gitlab.com/wrobell/rbfly/-/issues
#. View RbFly library source code: https://gitlab.com/wrobell/rbfly
#. RabbitMQ Streams: https://www.rabbitmq.com/streams.html
#. RabbitMQ Streams Java library:
   https://rabbitmq.github.io/rabbitmq-stream-java-client/stable/htmlsingle/
#. RabbitMQ discussions: https://groups.google.com/g/rabbitmq-users/

Acknowledgements
----------------
The design and implementation of RbFly library is inspired by other
projects

- `asyncpg <https://magicstack.github.io/asyncpg/current/>`_
- `rstream <https://github.com/qweeze/rstream>`_
- `aioredis <https://aioredis.readthedocs.io/en/latest/>`_
- `aiokafka <https://aiokafka.readthedocs.io/en/stable/>`_

.. vim: sw=4:et:ai
