.. todo: expand this document into tls topic article
         - generate tls certificates for development purposes 
         - configuring RabbitMQ Streams
         - making connection
         - refer to other articles for production level information

TLS Connection
==============
See also

- `RabbitMQ TLS Support <https://www.rabbitmq.com/ssl.html>`_
- `RabbitMQ Streams TLS Support <https://www.rabbitmq.com/stream.html#tls>`_
- :py:mod:`Python TLS/SSL Wrapper for Socket Objects <ssl>`

.. vim: sw=4:et:ai
