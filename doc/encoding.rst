.. _sect-encoding:

Message Data Types
==================
.. note::
   Support for AMQP 1.0 encoding and decoding is work in progress.

Message body, sent to a RabbitMQ stream, is any Python object having one of
the types

:py:class:`bytes`
    A data section of AMQP message containing opaque binary data. It can
    carry any binary message encoded with third-party encoders like
    `MessagePack <https://msgpack.org/>`_.

    The data type is also accepted as a member of a container type like
    sequence, or dictionary.
:py:data:`None`
    An AMQP message with null value.
:py:class:`str`
    An AMQP message with string value encoded with Unicode.
:py:class:`bool`
    An AMQP message with boolean value (true or false).
:py:class:`int`
    An AMQP message with integer value. The value is encoded with AMQP
    type as defined by the table

    ================================= ==============
    Value Range                       AMQP Type Name
    ================================= ==============
    [-2\ :sup:`31`, 2\ :sup:`31` - 1]      int
    [-2\ :sup:`63`, 2\ :sup:`63` - 1]     long
    [2\ :sup:`63`, 2\ :sup:`64` - 1]     ulong
    ================================= ==============

    All other, AMQP integer types are parsed as Python :py:class:`int`
    type.
:py:class:`float`
    An AMQP message with value of AMQP type double. AMQP type float is
    parsed as value of Python type float, as well.
sequence
    An AMQP message with value of AMQP type list. Python :py:class:`list`
    and :py:class:`tuple` types are supported when serializing to AMQP
    format. Always decoded into a Python list.
:py:class:`dict`
    An AMQP message with value of AMQP type map. The order of key-value
    pairs in a map is preserved on encoding and decoding. In AMQP standard,
    the order of key-value pairs is semantically important, but this is not
    the case for Python dictionary.
:py:class:`datetime.datetime`
    An AMQP message with value of AMQP type timestamp. Unaware datetime
    object is encoded as UTC timestamp value. Decoded datetime object has
    UTC timezone.
:py:class:`uuid.UUID`
    An AMQP message with an universally unique identifier as defined by
    RFC-4122, section 4.1.2.
:py:class:`~rbfly.streams.Symbol`
    Symbolic value from a constrained domain. The value is an ASCII string.

.. vim: sw=4:et:ai
