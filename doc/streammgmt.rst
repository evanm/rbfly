.. _sec-stream-manage:

Manage Streams
==============
There are multiple ways to manage RabbitMQ streams. The recommended way to
create or delete a RabbitMQ stream is to use RabbitMQ CLI tools or
infrastructure automation software.

RabbitMQ CLI Tools
------------------
Use `rabbitmqadmin <https://www.rabbitmq.com/management-cli.html>`_ and
`rabbitmqctl <https://www.rabbitmq.com/rabbitmqctl.8.html>`_ command-line
RabbitMQ tools to create and delete streams.

To create a RabbitMQ stream, declare a queue with `queue_type` argument set
to `stream`::

    $ rabbitmqadmin declare queue name=yellow.carrot queue_type=stream \
        arguments='{"x-max-age": "24h"}'

To delete a stream use `rabbitmqctl` command-line tool::

    $ rabbitmqctl delete_queue yellow.carrot

Automation Tools
----------------
RabbitMQ streams can be created using automation tools like `Terraform
<https://www.terraform.io/>`_ or `Ansible <https://www.ansible.com/>`_

- `Ansible RabbitMQ Queue Module
  <https://docs.ansible.com/ansible/latest/collections/community/rabbitmq/rabbitmq_queue_module.html>`_
- `Terraform RabbitMQ Queue Resource
  <https://registry.terraform.io/providers/cyrilgdn/rabbitmq/latest/docs/resources/queue>`_

As with RabbitMQ CLI tools, declare a RabbitMQ queue, and use
`x-queue-type` argument set to `stream`. For example, for Ansible:

.. code-block:: yaml

    - name: Create a RabbitMQ stream
      become: yes
      community.rabbitmq.rabbitmq_queue:
        name: yellow.carrot
        arguments:
          x-queue-type: stream
          x-max-age: 1D

RbFly API
---------
.. note::

   This method of creating and deleting RabbitMQ streams is not
   recommended. It is used for tesing and in short examples only.

RbFly allows to create and delete streams programmatically with
:py:meth:`~rbfly.streams.StreamsClient.create_stream` and
:py:meth:`~rbfly.streams.StreamsClient.delete_stream` methods::

    await client.create_stream('yellow.carrot')
    await client.delete_stream('yellow.carrot')

.. vim: sw=4:et:ai
