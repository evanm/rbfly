:orphan:

RbFly - RabbitMQ Client Library
===============================
.. raw:: latex

   \newpage
   \part{RbFly Library}
   \newpage

.. toctree::
   :caption: RbFly Library
   :numbered:
   :hidden:

   overview
   license
   quick
   publish
   subscribe
   connmgmt
   pubdedup
   streammgmt

.. raw:: latex

   \newpage
   \part{RbFly Library Reference}
   \newpage

.. toctree::
   :caption: RbFly Library Reference
   :numbered:
   :hidden:

   api
   conn
   encoding
   changelog

* :ref:`genindex`

.. vim: sw=4:et:ai
