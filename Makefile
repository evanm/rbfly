. PHONY: run-perf-test
.SECONDEXPANSION:

PERF_TEST_DATE = 2023-11-02
PERF_TEST_REPEAT = 100
PERF_TEST_DATA = $(wildcard doc/$(PERF_TEST_DATE)-perf-*.csv)
PERF_PLOT_SCRIPT = scripts/plot-perf-amqp-codec scripts/plot-perf-cmp scripts/plot-perf-pub-amqp
PERF_PLOT_FILE := $(PERF_TEST_DATA:%.csv=%.pdf) $(PERF_TEST_DATA:%.csv=%.png)

DOC_DEPENDENCIES = $(PERF_PLOT_FILE)

include .pymake.mk

SCRIPTS = scripts/rbfly-demo \
	scripts/rbfly-demo-dedup \
	scripts/rbfly-connect \
	scripts/rbfly-recv \
	scripts/rbfly-perf-send \
	scripts/rstream-perf-send \
	scripts/aiokafka-perf-send \
	scripts/perf-amqp-codec

DOC_DEST = wrobell@dcmod.org:~/public_html/$(PROJECT)

SOURCE_CYTHON = \
	rbfly/amqp/_message.pxd \
	rbfly/amqp/_message.pyx \
	rbfly/_buffer.pxd \
	rbfly/_buffer.pyx \
	rbfly/_codec.pxd \
	rbfly/_codec.pyx \
	rbfly/streams/_client.pyx \
	rbfly/streams/_codec.pyx \
	rbfly/streams/_mqueue.pyx

# to include script utilities module
export MYPYPATH=scripts

$(PERF_PLOT_FILE): $$(addsuffix .csv,$$(basename $$@)) $(PERF_PLOT_SCRIPT)
	s=$$(echo $< | sed 's/.*perf-\([a-z-]\+\)\.csv/\1/'); \
	    scripts/plot-perf-$$s $< $@
	install -D $@ build/$@

run-perf-test:
	# is rabbitmq running?
	socat /dev/null TCP:localhost:5672
	# performance cpu scaling governor needs to be enabled
	grep -q '^performance' /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor || exit 1
	# turbo needs to be disabled
	grep -q '^1' /sys/devices/system/cpu/intel_pstate/no_turbo || exit 1

	#
	# run tests
	#
	./scripts/run-perf-amqp-codec > doc/$(PERF_TEST_DATE)-perf-amqp-codec.csv
	./scripts/run-perf-cmp $(PERF_TEST_REPEAT) > doc/$(PERF_TEST_DATE)-perf-cmp.csv

	# run the binary only message publishing test, and compare with message
	# publising in amqp format from previous test
	./scripts/run-perf-pub-amqp $(PERF_TEST_REPEAT) > doc/$(PERF_TEST_DATE)-perf-pub-amqp.csv
	grep 'rbfly,amqp,' doc/$(PERF_TEST_DATE)-perf-cmp.csv >> doc/$(PERF_TEST_DATE)-perf-pub-amqp.csv

test-integration: .stamp-$(PROJECT)-$(VERSION)
	pytest --doctest-modules -vv --cov-fail-under=90 --cov=$(MODULE) $(MODULE) tests_int
