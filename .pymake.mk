# commands:
#
# 	doc
# 		generate documentation
# 	doc-upload
# 		upload documentation
# 	check-code
# 		run code quality analysis
# 	check-type
# 		run static type analysis
# 	test
# 		run unit tests
# 	clean
# 		remove build directories, rendered documentation, and other build
# 		related artefacts
#
# project configuration:
#
#   SCRIPTS
#       list of scripts, which are part of the project
#   DOC_DEST 
#       where to upload the documentation

.PHONY = build-dist build-ext doc doc-upload check test clean

PROJECT = $(shell grep '^name' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
VERSION = $(shell grep '^version' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
MODULE = $(shell echo $(PROJECT) | sed 's/-//g')
PKG_INFO = $(shell echo $(PROJECT) | sed 's/-/_/g').egg-info/PKG-INFO
EGG = $(shell echo $(PROJECT) | sed 's/-/_/g')

export PYTHONPATH=.

RSYNC = rsync -cav \
	--exclude=\*~ --exclude=.\* \
	--delete-excluded --delete-after \
	--no-owner --no-group \
	--progress --stats

doc: $(DOC_DEPENDENCIES) .stamp-sphinx

doc-upload:
	$(RSYNC) build/doc/ $(DOC_DEST)

clean:
	rm -rf .stamp-* .sphinx-stamp
	rm -rf $(PROJECT).egg-info
	rm -rf build/doc build/latex
	rm -rf .mypy_cache .pytest_cache .ruff_cache

build-ext:
	python setup.py build_ext --inplace

build-dist:
	python -m build -n -s -x

install-deps-build:
	pip install tomli  # remove for python 3.11
	pip install -U $$(python -c "import tomli as tl; print(' '.join(tl.load(open('pyproject.toml', 'rb'))['build-system']['requires']))")

check-type: .stamp-$(PROJECT)-$(VERSION)
	mypy --strict --scripts-are-modules --implicit-reexport $(MODULE) $(SCRIPTS) tests_int

check-code: .stamp-$(PROJECT)-$(VERSION)
	ruff check $(MODULE) $(SCRIPTS) tests_int
	cython-lint --no-pycodestyle $(SOURCE_CYTHON)
	bandit -c pyproject.toml -r $(MODULE) $(SCRIPTS)

test: .stamp-$(PROJECT)-$(VERSION)
	pytest --doctest-modules -vv --cov=$(MODULE) $(MODULE)

.stamp-sphinx: .stamp-$(PROJECT)-$(VERSION)
	sphinx-build -a -D exclude_patterns=rbfly.rst -b html -t html doc build/doc
	sphinx-build -a -D root_doc=rbfly -D exclude_patterns=index.rst,performance.rst -b latex doc build/latex
	# process performance report separately or figures are not numbered
	sphinx-build -a -D root_doc=performance -D exclude_patterns=index.rst,rbfly.rst -b latex doc build/latex
	make -C build/latex
	cp build/latex/*.pdf build/doc

.stamp-$(PROJECT)-$(VERSION): $(PKG_INFO)
	touch .stamp-$(PROJECT)-$(VERSION)

$(PKG_INFO): setup.cfg $(wildcard setup.py)
	python -m build -n -s -x

